from django.db import models

# Create your models here.
class user_database(models.Model):
    mobile_number=models.CharField(max_length=264)
    name=models.CharField(max_length=264)
    role=models.CharField(max_length=264)
    id=models.CharField(max_length=264,primary_key=True)
    email=models.CharField(max_length=264)

class teachers(models.Model):
    teacher_id=models.CharField(max_length=264)
    subject_id=models.CharField(max_length=264)

class subjects(models.Model):
    subject_name=models.CharField(max_length=264)
    subject_id=models.CharField(max_length=264)

class tests(models.Model):
    teacher_id=models.CharField(max_length=264)
    subject_id=models.CharField(max_length=264)
    test_name=models.CharField(max_length=264)
    test_id=models.CharField(max_length=264)
    knowledge_section=models.CharField(max_length=264)
    understandability_section=models.CharField(max_length=264)
    application_section=models.CharField(max_length=264)
    creativity_section=models.CharField(max_length=264)
    skills_section=models.CharField(max_length=264)
    conceptual_section=models.CharField(max_length=264)

class test_marks(models.Model):
    test_id=models.CharField(max_length=264)
    student_id=models.CharField(max_length=264)
    knowledge_section=models.CharField(max_length=264)
    understandability_section=models.CharField(max_length=264)
    application_section=models.CharField(max_length=264)
    creativity_section=models.CharField(max_length=264)
    skills_section=models.CharField(max_length=264)
    conceptual_section=models.CharField(max_length=264)

class student_subjects(models.Model):
    student_id=models.CharField(max_length=264)
    subject_id=models.CharField(max_length=264)
