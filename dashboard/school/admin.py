from django.contrib import admin
from school.models import *
# Register your models here.
admin.site.register(user_database)
admin.site.register(teachers)
admin.site.register(subjects)
admin.site.register(tests)
admin.site.register(student_subjects)
admin.site.register(test_marks)
