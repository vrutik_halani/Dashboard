from django.shortcuts import render
import io
import os
import datetime as dt
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from school.models import *
import json
# Create your views here.
login_name='-'
login_id='-'
login_role='-'

def index(request):
    return render(request,'index.html')

def check_login(request):
    global login_id,login_name,login_role
    mobile=request.POST.get('mobile','')
    otp=request.POST.get('otp','')
    try:
        oo=user_database.objects.get(mobile_number=mobile)
        login_name=oo.name
        login_id=oo.id
        login_role=oo.role

    except Exception as e:
        return render(request,'index.html')

    if login_role=='student':
        return HttpResponseRedirect("/home/student")

def student_home_1(request):
    global login_id,login_name,login_role
    if login_id=='-':
        return HttpResponseRedirect('/')
    sub_averages={}
    section_averages={'understandability_section':[0,0],'conceptual_section':[0,0],'creativity_section':[0,0],'skills_section':[0,0],'application_section':[0,0],'knowledge_section':[0,0]}
    stud_marks=[]
    g_subs=[]
    print(login_name)
    test_m=test_marks.objects.filter(student_id=login_id)
    all_subs=[]
    ff=student_subjects.objects.filter(student_id=login_id)
    for i in ff:
        sid=i.subject_id
        all_subs.append(sid)
        print("Here : ",sid)
        kk=subjects.objects.get(subject_id=sid)
        g_subs.append(kk.subject_name.upper())
    ct=1
    for i in test_m:
        a=[]
        q=[]
        q.append(ct)
        ct+=1
        tid=i.test_id
        jj=tests.objects.get(test_id=tid)
        tname=jj.test_name
        sid=jj.subject_id
        if sid in all_subs:
            jj=subjects.objects.get(subject_id=sid)
            sname=jj.subject_name
            q.append(sname.upper())
            q.append(tname.upper())
            if sname.upper() in sub_averages:
                a=[]
            else:
                sub_averages[sname.upper()]=[0,0]
            jj=tests.objects.get(test_id=i.test_id)
            print(jj)
            tot=0
            tot+=float(jj.knowledge_section)
            section_averages['knowledge_section'][1]+=float(jj.knowledge_section)
            tot+=float(jj.understandability_section)
            section_averages['understandability_section'][1]+=float(jj.understandability_section)
            tot+=float(jj.creativity_section)
            section_averages['creativity_section'][1]+=float(jj.creativity_section)
            tot+=float(jj.application_section)
            section_averages['application_section'][1]+=float(jj.application_section)
            tot+=float(jj.skills_section)
            section_averages['skills_section'][1]+=float(jj.skills_section)
            tot+=float(jj.conceptual_section)
            section_averages['conceptual_section'][1]+=float(i.conceptual_section)
            sub_averages[sname.upper()][1]+=tot
            marks=0
            marks+=float(i.knowledge_section)
            section_averages['knowledge_section'][0]+=float(i.knowledge_section)
            marks+=float(i.understandability_section)
            section_averages['understandability_section'][0]+=float(i.understandability_section)
            marks+=float(i.creativity_section)
            section_averages['creativity_section'][0]+=float(i.creativity_section)
            marks+=float(i.application_section)
            section_averages['application_section'][0]+=float(i.application_section)
            marks+=float(i.skills_section)
            section_averages['skills_section'][0]+=float(i.skills_section)
            marks+=float(i.conceptual_section)
            section_averages['conceptual_section'][0]+=float(i.conceptual_section)

            sub_averages[sname.upper()][0]+=marks
            q.append(marks)
            q.append(tot)
            perc=marks/tot
            perc*=100
            perc=str(perc)
            perc=perc[0:6]
            q.append(perc)
            stud_marks.append(q)
    a=[]
    ct=1
    for i in sub_averages:
        s=[]
        s.append(i)
        perc=sub_averages[i][0]/sub_averages[i][1]
        perc*=100
        perc=str(perc)
        perc=perc[0:6]
        s.append(perc)
        s.append("/widget/"+login_id+"/"+i.title())
        s.append(i.lower())
        a.append(s)
        s.append("card text-white bg-flat-color-"+str(ct%5))
        ct+=1
        if ct%5==0:
            ct+=1
    sub_averages=a
    to_pass=[]
    ct=1
    for i in section_averages:
        a=[]
        a.append(i.replace('_',' ').upper())
        perc=0
        try:
            perc=section_averages[i][0]/section_averages[i][1]
        except Exception as e:
            perc=0
        perc=str(perc*100)
        perc=perc[0:6]
        a.append(perc)
        a.append("card text-white bg-flat-color-"+str(ct%6))
        ct+=1
        if ct%6==0:
            ct+=1
        to_pass.append(a)
    print(to_pass)

    print("ok ",sub_averages)
    return render(request,'student_main.html',context={'name':login_name.upper(),'sub_average':sub_averages,'stud_marks':stud_marks,'student_chart':"/graph/student_chart/"+login_id,'d_subs':g_subs,'tiles':to_pass,'bar_chart':"/graph/bar_chart/"+login_id})


def jscharts_subject_marks(request,loginid,sname):
    global login_id
    if login_id=='-':
        return HttpResponseRedirect('/')
    sub_averages={}
    sub_averages['test_names']=[]
    sub_averages['marks']=[]
    print(sname)
    ff=subjects.objects.get(subject_name=sname)
    sid=ff.subject_id
    test_m=test_marks.objects.filter(student_id=loginid)
    for i in test_m:
        a=[]
        tid=i.test_id
        jj=tests.objects.get(test_id=tid)
        sid1=jj.subject_id
        if sid==sid1:
            sub_averages['test_names'].append(jj.test_name)
            jj=tests.objects.get(test_id=i.test_id)
            print(jj)
            tot=0
            tot+=float(jj.knowledge_section)
            tot+=float(jj.understandability_section)
            tot+=float(jj.creativity_section)
            tot+=float(jj.application_section)
            tot+=float(jj.skills_section)
            tot+=float(jj.conceptual_section)
            marks=0
            marks+=float(i.knowledge_section)
            marks+=float(i.understandability_section)
            marks+=float(i.creativity_section)
            marks+=float(i.application_section)
            marks+=float(i.skills_section)
            marks+=float(i.conceptual_section)
            perc=marks/tot
            perc*=100
            perc=str(perc)
            perc=perc[0:6]
            sub_averages['marks'].append(perc)
    return JsonResponse(sub_averages,safe=False)

def average_graph(request,loginid):
    global login_id
    if login_id=='-':
        return HttpResponseRedirect('/')
    subs=[]
    ff=student_subjects.objects.filter(student_id=loginid)
    for i in ff:
        subs.append(i.subject_id)
    test=[]
    stu_marks=[]
    ave_marks=[]
    for i in subs:
        try:
            avep=[]
            ff=tests.objects.filter(subject_id=i)
            gg=subjects.objects.get(subject_id=i)
            test.append(gg.subject_name.upper())
            stup=[]
            for j in ff:
                tid=j.test_id
                gg=test_marks.objects.filter(test_id=tid)
                ct=0
                tm=0
                stum=0
                avem=0
                tm+=float(j.knowledge_section)
                tm+=float(j.conceptual_section)
                tm+=float(j.creativity_section)
                tm+=float(j.skills_section)
                tm+=float(j.understandability_section)
                tm+=float(j.application_section)
                for k in gg:
                    ct+=1
                    if k.student_id==loginid:
                        stum+=float(k.knowledge_section)
                        stum+=float(k.conceptual_section)
                        stum+=float(k.creativity_section)
                        stum+=float(k.skills_section)
                        stum+=float(k.application_section)
                        stum+=float(k.understandability_section)

                    avem+=float(k.understandability_section)
                    avem+=float(k.skills_section)
                    avem+=float(k.application_section)
                    avem+=float(k.creativity_section)
                    avem+=float(k.conceptual_section)
                    avem+=float(k.knowledge_section)

                stum=stum/tm
                stum=stum*100

                avem=avem/(tm*ct)
                avem=avem*100

                stup.append(stum)
                avep.append(avem)

            stu_marks.append(sum(stup)/len(stup))
            ave_marks.append(sum(avep)/len(avep))
        except Exception as e:
            continue
    data={}
    data['test']=test
    data['stud']=stu_marks
    data['ave']=ave_marks
    return JsonResponse(data,safe=False)

def bar_chart(request,loginid):
    global login_id,login_name,login_role
    if login_id=='-':
        return HttpResponseRedirect('/')
    test={}
    stu_marks=[]
    ave_marks=[]
    tes_marks=[]
    set_marks=[]
    labels=[]
    obs=test_marks.objects.filter(student_id=login_id)
    for i in obs:
        tid=i.test_id
        tname=tests.objects.get(test_id=tid)
        tname=tname.test_name
        tname=tname.upper()
        if tname in test:
            continue
        else:
            test[tname]=1
    for i in test:
        obs=tests.objects.filter(test_name=i.lower())
        ostm=0
        oavm=0
        ototm=0
        labels.append(i)
        ct=0
        for j in obs:
            tid=j.test_id
            stm=0
            avm=0
            totm=0
            totm+=float(j.application_section)
            totm+=float(j.knowledge_section)
            totm+=float(j.creativity_section)
            totm+=float(j.conceptual_section)
            totm+=float(j.skills_section)
            totm+=float(j.understandability_section)
            ct+=1
            all_obs=test_marks.objects.filter(test_id=tid)
            for k in all_obs:
                a=0
                a+=float(k.application_section)
                a+=float(k.knowledge_section)
                a+=float(k.creativity_section)
                a+=float(k.conceptual_section)
                a+=float(k.skills_section)
                a+=float(k.understandability_section)
                avm+=a
                if k.student_id==login_id:
                    stm+=a
            ostm+=stm
            oavm+=avm
            ototm+=totm
        stu_marks.append(ostm)
        ave_marks.append(oavm)
        tes_marks.append(ototm)
        set_marks.append(ototm*ct)

    for i in range(len(stu_marks)):
        stu_marks[i]=(stu_marks[i]/tes_marks[i])*100
        ave_marks[i]=(ave_marks[i]/set_marks[i])*100

    data={}
    data['labels']=labels
    data['stu']=stu_marks
    data['ave']=ave_marks
    print(data)
    return JsonResponse(data,safe=False)

def subject_page(request,sub):
    global login_id
    if login_id=='-':
        return HttpResponseRedirect('/')
    sub=sub.title()

    g_subs=[]
    g_sid='-'
    ff=student_subjects.objects.filter(student_id=login_id)
    for i in ff:
        sid=i.subject_id
        kk=subjects.objects.get(subject_id=sid)
        if kk.subject_name==sub:
            g_sid=sid
        g_subs.append(kk.subject_name.upper())
    f=1
    tiles=[]
    tiles.append([])
    tiles.append([])
    tiles.append([])
    tiles.append([])
    tiles.append([])
    tiles.append([])
    tes_marks=[]
    oct=0
    ct=1
    try:
        print(login_id,g_sid)
        obs=tests.objects.filter(subject_id=g_sid)
        for i in obs:
            print(i)
            a=[]
            a.append(i.test_name.upper())
            tid=i.test_id
            totm=0
            stum=0
            totm+=float(i.understandability_section)
            totm+=float(i.conceptual_section)
            totm+=float(i.creativity_section)
            totm+=float(i.skills_section)
            totm+=float(i.application_section)
            totm+=float(i.knowledge_section)
            try:
                ff=test_marks.objects.get(test_id=tid,student_id=login_id)
                stum+=float(ff.understandability_section)
                tiles[0].append(float(ff.understandability_section)/float(i.understandability_section))
                stum+=float(ff.conceptual_section)
                tiles[1].append(float(ff.conceptual_section)/float(i.conceptual_section))
                stum+=float(ff.creativity_section)
                tiles[2].append(float(ff.creativity_section)/float(i.creativity_section))
                stum+=float(ff.skills_section)
                tiles[3].append(float(ff.skills_section)/float(i.skills_section))
                stum+=float(ff.application_section)
                tiles[4].append(float(ff.application_section)/float(i.application_section))
                stum+=float(ff.knowledge_section)
                tiles[5].append(float(ff.knowledge_section)/float(i.knowledge_section))
                x=(stum/totm)*100
                x=str(x)
                x=x[0:6]
                a.append(x)
                
                a.append("card text-white bg-flat-color-"+str(ct%5))
                tes_marks.append(a)
                oct+=1
                ct+=1
                if ct%5==0:
                    ct+=1
            except Exception as e:
                print(1)


    except Exception as e:
        f=0
    if oct==0:
        return render(request,'subject_page.html',context={'d_subs':g_subs,'name':login_name.upper(),'subject':sub,'test_average':tes_marks,'tiles':tiles,'nothing':'1'})

    for i in range(len(tiles)):
        x=sum(tiles[i])/len(tiles[i])
        x=x*100
        x=str(x)
        x=x[0:6]
        tiles[i]=[0,x]
    print(tiles)
    print(tes_marks)
    link="/subject_student_chart/"+sub+"/"+login_id
    print("link = ",link)
    return render(request,'subject_page.html',context={'d_subs':g_subs,'name':login_name.upper(),'subject':sub,'test_average':tes_marks,'tiles':tiles,'url':link})

def subject_student_chart(request,sub,loginid):
    global login_id
    if login_id=='-':
        return HttpResponseRedirect('/')
    print("Requested")
    g_sid=subjects.objects.get(subject_name=sub)
    g_sid=g_sid.subject_id
    login_id=loginid
    tes_marks=[]
    oct=0
    ct=1
    try:
        print("hello",login_id,g_sid)
        obs=tests.objects.filter(subject_id=g_sid)
        for i in obs:
            print(i)
            a=[]
            a.append(i.test_name.upper())
            tid=i.test_id
            totm=0
            stum=0
            avem=0
            totm+=float(i.understandability_section)
            totm+=float(i.conceptual_section)
            totm+=float(i.creativity_section)
            totm+=float(i.skills_section)
            totm+=float(i.application_section)
            totm+=float(i.knowledge_section)
            tc=0
            try:
                fff=test_marks.objects.filter(test_id=tid)
                for ff in fff:
                    qw=0
                    qw+=float(ff.understandability_section)
                    qw+=float(ff.conceptual_section)
                    qw+=float(ff.creativity_section)
                    qw+=float(ff.skills_section)
                    qw+=float(ff.application_section)
                    qw+=float(ff.knowledge_section)
                    tc+=1
                    if ff.student_id==login_id:
                        stum+=qw
                    avem+=qw
                a.append((stum/totm)*100)
                a.append((avem/(tc*totm))*100)
                tes_marks.append(a)
            except Exception as e:
                print(1)


    except Exception as e:
        f=0
    ll=[]
    stu=[]
    ave=[]
    for i in tes_marks:
        ll.append(i[0])
        stu.append(i[1])
        ave.append(i[2])
    data={}
    data['labels']=ll
    data['stu']=stu
    data['ave']=ave
    return JsonResponse(data,safe=False)

def logout(request):
    global login_id,login_name,login_role
    login_name='-'
    login_id='-'
    login_role='-'
    return HttpResponseRedirect('/')

def test_page(request,sub,test):
    global login_id,login_name,login_role
    if login_id=='-':
        return HttpResponseRedirect('/')
    g_subs=[]
    g_sid='-'
    ff=student_subjects.objects.filter(student_id=login_id)
    for i in ff:
        sid=i.subject_id
        kk=subjects.objects.get(subject_id=sid)
        if kk.subject_name==sub:
            g_sid=sid
        g_subs.append(kk.subject_name.upper())
    context={}
    context['subject']=sub
    context['test_name']=test
    context['name']=login_name.upper()
    context['d_subs']=g_subs
    sid=subjects.objects.get(subject_name=sub)
    sid=sid.subject_id
    test=test.lower()
    print(sid,test)
    obj=tests.objects.get(subject_id=sid,test_name=test)
    tiles=[]
    tid=obj.test_id
    ct=0
    stum=[0,0,0,0,0,0]
    avem=[0,0,0,0,0,0]
    totm=[0,0,0,0,0,0]
    totm[0]=float(obj.understandability_section)
    totm[2]=float(obj.creativity_section)
    totm[1]=float(obj.conceptual_section)
    totm[3]=float(obj.skills_section)
    totm[4]=float(obj.application_section)
    totm[5]=float(obj.knowledge_section)
    fff=test_marks.objects.filter(test_id=tid)
    for ff in fff:
        qw=[0,0,0,0,0,0]
        qw[0]=float(ff.understandability_section)
        qw[1]=float(ff.conceptual_section)
        qw[2]=float(ff.creativity_section)
        qw[3]=float(ff.skills_section)
        qw[4]=float(ff.application_section)
        qw[5]=float(ff.knowledge_section)
        ct+=1
        if ff.student_id==login_id:
            stum=qw
        for i in range(len(qw)):
            avem[i]+=qw[i]
    tiles=[]
    for i in range(len(stum)):
        x=stum[i]/totm[i]
        x=x*100
        x=str(x)
        x=x[0:6]
        tiles.append([0,x])
    tots=sum(stum)/sum(totm)
    tots*=100
    context['tiles']=tiles
    context['total']=tots
    context['link']="/student_test_chart/"+sub+"/"+test+"/"+login_id
    return render(request,'test_page.html',context=context)

def student_test_graph(request,sub,test,loginid):
    global login_id,login_name,login_role
    if login_id=='-':
        return HttpResponseRedirect('/')
    g_subs=[]
    g_sid='-'
    ff=student_subjects.objects.filter(student_id=login_id)
    for i in ff:
        sid=i.subject_id
        kk=subjects.objects.get(subject_id=sid)
        if kk.subject_name==sub:
            g_sid=sid
        g_subs.append(kk.subject_name.upper())
    context={}
    context['subject']=sub
    context['test_name']=test
    context['name']=login_name
    context['d_subs']=g_subs
    sid=subjects.objects.get(subject_name=sub)
    sid=sid.subject_id
    test=test.lower()
    print(sid,test)
    obj=tests.objects.get(subject_id=sid,test_name=test)
    tiles=[]
    tid=obj.test_id
    ct=0
    stum=[0,0,0,0,0,0]
    avem=[0,0,0,0,0,0]
    totm=[0,0,0,0,0,0]
    totm[0]=float(obj.understandability_section)
    totm[2]=float(obj.creativity_section)
    totm[1]=float(obj.conceptual_section)
    totm[3]=float(obj.skills_section)
    totm[4]=float(obj.application_section)
    totm[5]=float(obj.knowledge_section)
    fff=test_marks.objects.filter(test_id=tid)
    for ff in fff:
        qw=[0,0,0,0,0,0]
        qw[0]=float(ff.understandability_section)
        qw[1]=float(ff.conceptual_section)
        qw[2]=float(ff.creativity_section)
        qw[3]=float(ff.skills_section)
        qw[4]=float(ff.application_section)
        qw[5]=float(ff.knowledge_section)
        ct+=1
        if ff.student_id==login_id:
            stum=qw
        for i in range(len(qw)):
            avem[i]+=qw[i]
    for i in range(len(stum)):
        stum[i]=(stum[i]/totm[i])*100
        avem[i]=(avem[i]/(ct*totm[i]))*100
    data={}
    data['labels']=['Understandability Section','Conceptual Section','Creativity Section','Skills Section','Application Section','Knowledge Section']
    data['stu']=stum
    data['ave']=avem
    return JsonResponse(data,safe=False)
