"""dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from school import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('check_login',views.check_login),
    path('',views.index),
    path("widget/<loginid>/<sname>",views.jscharts_subject_marks),
    path('home/student',views.student_home_1),
    path('graph/student_chart/<loginid>',views.average_graph),
    path('graph/bar_chart/<loginid>',views.bar_chart),
    path('subject/<sub>',views.subject_page),
    path('subject_student_chart/<sub>/<loginid>',views.subject_student_chart),
    path('test/<sub>/<test>',views.test_page),
    path('student_test_chart/<sub>/<test>/<loginid>',views.student_test_graph),
    path('logout',views.logout)
]
