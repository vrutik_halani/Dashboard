( function ( $ ) {
    "use strict";


    // Counter Number
    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 3000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

    //WidgetChart 1
    var ctx = document.getElementById("science");
    var link=document.getElementById("science_link");
    ctx.height = 150;
    var url=link.value;
    console.log(url)
    var xmlhttp = new XMLHttpRequest();
    window.labs="hi"
    window.dats="hello"
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            window.labs=myArr.test_names;
            window.dats=myArr.marks;
            doit();
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    function doit(){
        console.log(window.labs);
    var myChart = new Chart( ctx, {
        type: 'line',
        data: {
            labels: window.labs,
            type: 'line',
            datasets: [ {
                data: window.dats,
                label: 'Percentage',
                backgroundColor: 'transparent',
                borderColor: 'rgba(255,255,255,.55)',
            }, ]
        },
        options: {

            maintainAspectRatio: false,
            legend: {
                display: false
            },
            responsive: true,
            scales: {
                xAxes: [ {
                    gridLines: {
                        color: 'transparent',
                        zeroLineColor: 'transparent'
                    },
                    ticks: {
                        fontSize: 2,
                        fontColor: 'transparent'
                    }
                } ],
                yAxes: [ {
                    display:false,
                    ticks: {
                        display: false,
                    }
                } ]
            },
            title: {
                display: false,
            },
            elements: {
                line: {
                    borderWidth: 1
                },
                point: {
                    radius: 4,
                    hitRadius: 10,
                    hoverRadius: 4
                }
            }
        }
    } );
}
  //maths
  var ctx1 = document.getElementById("maths");
  var link=document.getElementById("maths_link");
  ctx.height = 150;
  var url=link.value;
  console.log(url)
  var xmlhttp = new XMLHttpRequest();
  window.labs="hi"
  window.dats="hello"
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var myArr = JSON.parse(this.responseText);
          window.labs=myArr.test_names;
          window.dats=myArr.marks;
          doit1();
      }
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
  function doit1(){
      console.log(window.labs);
  var myChart = new Chart( ctx1, {
      type: 'line',
      data: {
          labels: window.labs,
          type: 'line',
          datasets: [ {
              data: window.dats,
              label: 'Percentage',
              backgroundColor: 'transparent',
              borderColor: 'rgba(255,255,255,.55)',
          }, ]
      },
      options: {

          maintainAspectRatio: false,
          legend: {
              display: false
          },
          responsive: true,
          scales: {
              xAxes: [ {
                  gridLines: {
                      color: 'transparent',
                      zeroLineColor: 'transparent'
                  },
                  ticks: {
                      fontSize: 2,
                      fontColor: 'transparent'
                  }
              } ],
              yAxes: [ {
                  display:false,
                  ticks: {
                      display: false,
                  }
              } ]
          },
          title: {
              display: false,
          },
          elements: {
              line: {
                  borderWidth: 1
              },
              point: {
                  radius: 4,
                  hitRadius: 10,
                  hoverRadius: 4
              }
          }
      }
  } );
}

var ctx2 = document.getElementById("english");
var link2=document.getElementById("english_link");
ctx2.height = 150;
var url2=link2.value;
console.log(url2)
var xmlhttp2 = new XMLHttpRequest();
window.labs="hi"
window.dats="hello"
xmlhttp2.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        window.labs=myArr.test_names;
        window.dats=myArr.marks;
        doit2();
    }
};
xmlhttp2.open("GET", url2, true);
xmlhttp2.send();
function doit2(){
    console.log(window.labs);
var myChart2 = new Chart( ctx2, {
    type: 'line',
    data: {
        labels: window.labs,
        type: 'line',
        datasets: [ {
            data: window.dats,
            label: 'Percentage',
            backgroundColor: 'transparent',
            borderColor: 'rgba(255,255,255,.55)',
        }, ]
    },
    options: {

        maintainAspectRatio: false,
        legend: {
            display: false
        },
        responsive: true,
        scales: {
            xAxes: [ {
                gridLines: {
                    color: 'transparent',
                    zeroLineColor: 'transparent'
                },
                ticks: {
                    fontSize: 2,
                    fontColor: 'transparent'
                }
            } ],
            yAxes: [ {
                display:false,
                ticks: {
                    display: false,
                }
            } ]
        },
        title: {
            display: false,
        },
        elements: {
            line: {
                borderWidth: 1
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4
            }
        }
    }
} );
}
} )( jQuery );
