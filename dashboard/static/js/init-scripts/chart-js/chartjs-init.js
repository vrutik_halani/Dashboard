( function ( $ ) {
    "use strict";

    //Student vs Ave chart
    console.log("Hello");
    var ctx = document.getElementById( "student-chart" );
    if(ctx!=null){
    ctx.height=100;
    var link = document.getElementById("link");
    link=link.value;
    var url =link;
    var xmlhttp = new XMLHttpRequest();
    window.labs="hi"
    window.dats="hello"
    window.tests="ok"
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            window.labs=myArr.test;
            window.dats=myArr.stud;
            window.tests=myArr.ave;
            console.log(window.dats);
            console.log(window.labs);
            console.log(window.dats);
            qwerty();
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    function qwerty(){
    var myChart = new Chart( ctx, {
        type: 'bar',
        data: {
            labels:window.labs,
            datasets: [
                {
                    label: "Student Marks",
                    data: window.dats,
                    borderColor: "rgba(0, 123, 255, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 123, 255, 0.5)"
                            },
                {
                    label: "Average Marks",
                    data: window.tests,
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0,0,0,0.07)"
                            }
                        ]
        },
        options: {
            scales: {
                yAxes: [ {
                  scaleLabel: {
                      display: true,
                      labelString: 'Percentage'
                  },
                  ticks: {
                              beginAtZero: true,
                              steps: 10,
                              stepValue: 5,
                              max: 100
                          }
                                } ]
            }
        }
    } );
  }

}
  //Bar chart
  console.log("Hi");
  var ctx1 = document.getElementById( "barChart" );
  if(ctx1!=null){
  ctx1.height = 100;
  var link1=document.getElementById("url");
  link1=link1.value;
  var url1 =link1;
  var xmlhttp1 = new XMLHttpRequest();
  window.labels="hi"
  window.stu="hello"
  window.ave="ok"
  xmlhttp1.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var myArr = JSON.parse(this.responseText);
          console.log(myArr);
          window.labels=myArr.labels;
          window.stu=myArr.stu;
          window.ave=myArr.ave;
          console.log(myArr.labels);
          console.log(myArr.stu);
          doit();
      }
  };
  xmlhttp1.open("GET", url1, true);
  xmlhttp1.send();
  function doit(){
  var myChart = new Chart( ctx1, {
      type: 'line',
      data: {
          labels: window.labels,
          type: 'line',
          defaultFontFamily: 'Montserrat',
          datasets: [ {
              label: "Student",
              data: window.stu,
              backgroundColor: 'transparent',
              borderColor: 'rgba(220,53,69,0.75)',
              borderWidth: 3,
              pointStyle: 'circle',
              pointRadius: 5,
              pointBorderColor: 'transparent',
              pointBackgroundColor: 'rgba(220,53,69,0.75)',
                  }, {
              label: "Class Average",
              data: window.ave,
              backgroundColor: 'transparent',
              borderColor: 'rgba(40,167,69,0.75)',
              borderWidth: 3,
              pointStyle: 'circle',
              pointRadius: 5,
              pointBorderColor: 'transparent',
              pointBackgroundColor: 'rgba(40,167,69,0.75)',
            }  ]
      },
      options: {
          responsive: true,

          tooltips: {
              mode: 'index',
              titleFontSize: 12,
              titleFontColor: '#000',
              bodyFontColor: '#000',
              backgroundColor: '#fff',
              titleFontFamily: 'Montserrat',
              bodyFontFamily: 'Montserrat',
              cornerRadius: 3,
              intersect: false,
          },
          legend: {
              display: false,
              labels: {
                  usePointStyle: true,
                  fontFamily: 'Montserrat',
              },
          },
          scales: {
              xAxes: [ {
                  display: true,
                  gridLines: {
                      display: false,
                      drawBorder: false
                  },
                  scaleLabel: {
                      display: false
                  }
                      } ],
              yAxes: [ {
                  display: true,
                  gridLines: {
                      display: false,
                      drawBorder: false
                  },
                  scaleLabel: {
                      display: true,
                      labelString: 'Percentage'
                  },
                  ticks: {
                              beginAtZero: true,
                              steps: 10,
                              stepValue: 5,
                              max: 100
                          }
                      } ]
          },
          title: {
              display: false,
              text: 'Normal Legend'
          }
      }
  } );

}
}
var ctx3 = document.getElementById( "test_student_chart" );
if(ctx3!=null){
ctx3.height=100;
console.log(ctx3.height);
var link3 = document.getElementById("url");
link3=link3.value;
console.log(link3);
var url3 =link3;
console.log(url3);
var xmlhttp3 = new XMLHttpRequest();
window.ll="hi"
window.stu="hello"
window.ave="ok"
xmlhttp3.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        window.ll=myArr.labels;
        window.stu=myArr.stu;
        window.ave=myArr.ave;
        console.log(window.ll);
        console.log(window.stu);
        doit3();
    }
};
xmlhttp3.open("GET", url3, true);
xmlhttp3.send();
function doit3(){
var myChart = new Chart( ctx3, {
    type: 'line',
    data: {
        labels: window.ll,
        type: 'line',
        defaultFontFamily: 'Montserrat',
        datasets: [ {
            label: "Student",
            data: window.stu,
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
                }, {
            label: "Class Average",
            data: window.ave,
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
          }  ]
    },
    options: {
        responsive: true,

        tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Montserrat',
            bodyFontFamily: 'Montserrat',
            cornerRadius: 3,
            intersect: false,
        },
        legend: {
            display: false,
            labels: {
                usePointStyle: true,
                fontFamily: 'Montserrat',
            },
        },
        scales: {
            xAxes: [ {
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                scaleLabel: {
                    display: false
                }
                    } ],
            yAxes: [ {
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Percentage'
                },
                ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        }
                    } ]
        },
        title: {
            display: false,
            text: 'Normal Legend'
        }
    }
} );

}
}
var ctx2 = document.getElementById( "subject_student_chart" );
if(ctx2!=null){
ctx2.height=100;
console.log(ctx2.height);
var link2 = document.getElementById("url");
link2=link2.value;
console.log(link2);
var url2 =link2;
console.log(url2);
var xmlhttp2 = new XMLHttpRequest();
window.ll="hi"
window.stu="hello"
window.ave="ok"
xmlhttp2.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        window.ll=myArr.labels;
        window.stu=myArr.stu;
        window.ave=myArr.ave;
        console.log(window.ll);
        console.log(window.stu);
        doit3();
    }
};
xmlhttp2.open("GET", url2, true);
xmlhttp2.send();
function doit3(){
var myChart = new Chart( ctx2, {
    type: 'line',
    data: {
        labels: window.ll,
        type: 'line',
        defaultFontFamily: 'Montserrat',
        datasets: [ {
            label: "Student",
            data: window.stu,
            backgroundColor: 'transparent',
            borderColor: 'rgba(220,53,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(220,53,69,0.75)',
                }, {
            label: "Class Average",
            data: window.ave,
            backgroundColor: 'transparent',
            borderColor: 'rgba(40,167,69,0.75)',
            borderWidth: 3,
            pointStyle: 'circle',
            pointRadius: 5,
            pointBorderColor: 'transparent',
            pointBackgroundColor: 'rgba(40,167,69,0.75)',
          }  ]
    },
    options: {
        responsive: true,

        tooltips: {
            mode: 'index',
            titleFontSize: 12,
            titleFontColor: '#000',
            bodyFontColor: '#000',
            backgroundColor: '#fff',
            titleFontFamily: 'Montserrat',
            bodyFontFamily: 'Montserrat',
            cornerRadius: 3,
            intersect: false,
        },
        legend: {
            display: false,
            labels: {
                usePointStyle: true,
                fontFamily: 'Montserrat',
            },
        },
        scales: {
            xAxes: [ {
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                scaleLabel: {
                    display: false
                }
                    } ],
            yAxes: [ {
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Percentage'
                },
                ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        }
                    } ]
        },
        title: {
            display: false,
            text: 'Normal Legend'
        }
    }
} );

}
}



} )( jQuery );
